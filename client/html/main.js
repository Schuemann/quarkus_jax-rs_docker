    var app = new Vue({
    el: '#app',
    data: {
        product: 'Socks',
        image: '/assets/vmSocks-green-onWhite.jpg',
        inventory: 1, 
        inStock: true,
        details: ["80% Cotton", "Bio", "Fair"],
        cart: 0
    },
    methods: {
        addToCart: function() {
            this.cart += 1
        },
        removeFromCart: function() {
            if(this.cart > 0) {
                this.cart -= 1
            }
        }
    }
})