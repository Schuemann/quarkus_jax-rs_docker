import entities.Event;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Testing Entity-class Event
 * */

@QuarkusTest
public class EventTest {

    static Event testEvent;
    static String hashId;

    Logger logger = Logger.getLogger(getClass().getName());

    @BeforeAll
    public static void init(){
        try {
            Date startTime = new GregorianCalendar(2020, 1, 1, 12, 30).getTime();
            Date endTime = new GregorianCalendar(2020, 1, 1, 13, 30).getTime();
            testEvent = new Event("Event name", "description", startTime, endTime);
            hashId = "f0f7660a5e9c93f2b59adccdaaebaddb10e13d7219e4ebbf607c218b218d84d30a4d5cc20367b45306a93f235d4cb40b53bb1be510d8d09f54dca31b6d7c528c";
        } catch (Exception e) {
            Logger.getLogger(EventTest.class.getName()).log(Level.SEVERE, "Error while initiating test-class", e);
        }
    }

    @Test
    public void testCreateHashId() {
        try {
            String hashId = testEvent.createHashId();
            Assertions.assertTrue(EventTest.hashId.equals(hashId));
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Exception testing testCreateHashId", e);
        }
    }

}
