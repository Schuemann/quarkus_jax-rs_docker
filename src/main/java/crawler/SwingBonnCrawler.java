package crawler;

import business_logic.utils.DateUtils;
import entities.Event;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import javax.enterprise.context.ApplicationScoped;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@ApplicationScoped
public class SwingBonnCrawler implements Crawler {

    Logger logger = Logger.getLogger(getClass().getName());

    @Override
    public List<Event> fetchEvents(String fetchFromUrl) throws Exception {
       return fetchEventsJSoup(fetchFromUrl);
    }

    private List<Event> fetchEvents() {
        /* TODO: https://stackoverflow.com/questions/3142915/how-do-you-create-an-asynchronous-http-request-in-java#3143189
        * See part: "you are in a JEE7 environment, you must have a decent implementation of JAXRS hanging around, which would allow you to easily make asynchronous HTTP request using its client API."
        *
        * */
        return null;
    }

    private List<Event> fetchEventsJSoup(String fetchFromUrl) throws Exception {
        String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.112 Safari/535.1";
        Connection connection = Jsoup.connect(fetchFromUrl).userAgent(USER_AGENT);
        Document htmlDocument = connection.get();
        Element font = htmlDocument.getElementsByClass("wsite-content-title").get(1).getElementsByTag("font").get(0);
        String html = font.html();
        String[] rawEntries = html.split("<br");
        List<Event> cleanEntries = new ArrayList<>();
        for (String s: rawEntries) {
            String cleanString = s.replaceAll("><strong>", "")
                    .replace("<strong>", "")
                    .replace("&nbsp;", "")
                    .replace("</strong>&nbsp;", "")
                    .replace("</strong>", "");
            System.out.println(cleanString);
            String[] eventInfos = cleanString.split(" - ");
            if(eventInfos.length > 3) {
                throw new Exception("no correct format");
            } else {
                String eventName = eventInfos[0];
                StartStopTimes startStopTimes;
                String eventDescription = eventInfos[2];
                try {
                    startStopTimes = bonnSwingDatetoDate(eventInfos[1]);
                } catch (Exception e) {
                    logger.log(Level.WARNING, "Could not extract start/stop-times '" + eventInfos[1] + "' from event - Event not created ");
                    continue; // do not create Events without date!
                }
                Event event = new Event(eventName, eventDescription, startStopTimes.startTime, startStopTimes.endTime);
                cleanEntries.add(event);
            }
        }
        return cleanEntries;
    }

    private StartStopTimes bonnSwingDatetoDate(String swingDate) throws Exception {
        StartStopTimes startStopTime = null;
        String[] dateParts = swingDate.split(" ");
        if(dateParts.length != 5) {
        //    StringBuilder nonParseableDate = new StringBuilder();
          //  for(String part : dateParts) nonParseableDate.append(part + " ");
            throw new Exception("could not parse the Date of following ");
        } else {
            String dayOfWeek = dateParts[0];
            String dayOfMonth = dateParts[1].replace(".", "");
            String monthOfYear = dateParts[2];
            String timeStart = dateParts[3];
            String timeEnd = dateParts[4];
            Date startTime = new GregorianCalendar(
                    2020,
                    DateUtils.getMonthNumberForMonthNameDE(monthOfYear),
                    Integer.parseInt(dayOfMonth),
                    Integer.parseInt(timeStart.split(":")[0]),
                    0).getTime();
            Date endTime = new GregorianCalendar(
                    2020,
                    DateUtils.getMonthNumberForMonthNameDE(monthOfYear),
                    Integer.parseInt(dayOfMonth),
                    Integer.parseInt(timeEnd.split(":")[0]),
                    0).getTime();
            startStopTime = new StartStopTimes(startTime, endTime);
        }
        return startStopTime;
    }

    private class StartStopTimes {
        Date startTime;
        Date endTime;

        StartStopTimes(Date startTime, Date stopTime){
            this.startTime = startTime;
            this.endTime = stopTime;
        }
    }

}
