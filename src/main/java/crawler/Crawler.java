package crawler;

import entities.Event;

import javax.transaction.Transactional;
import java.util.List;

public interface Crawler {

    @Transactional
    List<Event> fetchEvents(String fetchFromUrl) throws Exception;

}
