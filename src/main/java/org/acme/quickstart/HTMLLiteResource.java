package org.acme.quickstart;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("/someHTML")
public class HTMLLiteResource {

    Logger logger = Logger.getLogger(getClass().getName());

    @GET
    @Produces(MediaType.TEXT_HTML) // returns, if localhost/hello is called
    public InputStream hello() throws WebApplicationException {
        String path = System.getProperty("user.dir") + "/classes/2020-02-09-vue-starting.html";
        File f = new File(path);
        try {
            return new FileInputStream(f);
        } catch (FileNotFoundException e) {
            logger.log(Level.SEVERE, "hello went wrong", e);
        }
        return null;
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("/testHTMLAndCSS")
    public InputStream testingHtmlAndCSS() throws WebApplicationException {
        String path = System.getProperty("user.dir") + "/classes/webapp/html_css.html";
        File f = new File(path);
        try {
            return new FileInputStream(f);
        } catch (FileNotFoundException e) {
            logger.log(Level.SEVERE, "hello went wrong", e);
        }
        return null;
    }

}