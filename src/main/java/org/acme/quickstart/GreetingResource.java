package org.acme.quickstart;

import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Path("/hello")
public class GreetingResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN) // returns, if localhost/hello is called
    public String hello() {
        return "hello, hello-root-ressource here, please go deeper";
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN) // returns, if localhost/hello/greeting is called
    @Path("/greeting/{name}")
    public String greeting(@PathParam String name) {
        return "hey,  " + name + " on quarkus";
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/greetingAsyncAsocial/{greeting}")
    public CompletionStage<String> greetingAsyncAsocial() {
        return CompletableFuture.supplyAsync(() -> {
            return "Hello " + "F@*!@$er";
        });
    }
}