package entities;


import javax.persistence.*;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

/**
 *  Entity only to use with business logic and database-transactions! DO NOT use this class in your API -  Use DTO-Classes instead for your API
 * */

@Entity
@NamedQueries({
        @NamedQuery(name = "Event.findAll", query = "Select e from Event e"),
        @NamedQuery(name = "Event.getByEventHashes", query = "Select e from Event e where e.eventHash IN :eventHashes"),
        @NamedQuery(name = "Event.findByEventHash", query = "Select e from Event e where e.eventHash = :eventHash")
})
public class Event implements Serializable {

    Long id;
    String name;
    String description;
    Date dateStart;
    Date dateEnd;
    String eventHash;

    // ONLY use for serialisation
    public Event() { }

    public Event(String name, String description, Date dateStart, Date dateEnd) throws Exception {
        this.name = name;
        this.description = description;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.eventHash = createHashId();
    }

    public Event(Long id, String name, String description, Date dateStart, Date dateEnd) throws Exception {
        this.id = id;
        this.name = name;
        this.description = description;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.eventHash = createHashId();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="eventSeq")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getEventHash() {
        return eventHash;
    }

    public void setEventHash(String eventHash) {
        this.eventHash = eventHash;
    }

    @Transient
    public String createHashId() throws Exception {
        String toHash = name + description + dateStart + dateEnd;
        MessageDigest md = MessageDigest.getInstance("SHA-512");
        md.update("salt".getBytes(StandardCharsets.UTF_8));
        byte[] bytes = md.digest(toHash.getBytes(StandardCharsets.UTF_8));
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }
}
