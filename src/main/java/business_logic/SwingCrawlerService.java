package business_logic;

import crawler.SwingBonnCrawler;
import entities.Event;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class SwingCrawlerService {

    @Inject
    EntityManager entityManager;
    @Inject
    EventService eventService;

    @Transactional
    public List<Event> crawlEventSites() throws Exception {
        List<Event> crawledEvents = new SwingBonnCrawler().fetchEvents("https://www.lindy-hop-bonn.de/training--party.html");

        // prevent same events from creating again
        List<String> eventHashes = crawledEvents.stream().map(event -> event.getEventHash()).collect(Collectors.toList());
        List<Event> sameOldEvents = eventService.getByEventHashes(eventHashes);

        List<Event> eventsToPersist = new ArrayList<>();
        for (Event crawledEvent : crawledEvents) {
            Event eventToUpdate = null;
            boolean alreadyInDB = false;
            for (Event sameOldEvent : sameOldEvents) {
                if(sameOldEvent.getEventHash().equals(crawledEvent.getEventHash())){
                    alreadyInDB = true;
                }
            }
            if(!alreadyInDB) {
                eventsToPersist.add(crawledEvent);
            }
        }
        return eventsToPersist;
    }

}