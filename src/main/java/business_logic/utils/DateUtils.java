package business_logic.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DateUtils {

    public static Integer getMonthNumberForMonthNameDE(String monthName) throws ParseException {
        Date date = new SimpleDateFormat("MMMM", Locale.GERMAN).parse(monthName);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.MONTH);
    }

    public static void main(String[] args) {
        try {
            Integer dezember = getMonthNumberForMonthNameDE("Dezember");
        } catch (ParseException e) {
            Logger.getLogger(DateUtils.class.getName()).log(Level.SEVERE, "error parsing", e);
        }
    }

}
