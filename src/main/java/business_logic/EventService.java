package business_logic;

import entities.Event;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NamedQuery;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.TransactionScoped;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationScoped
public class EventService {

    @Inject
    EntityManager entityManager;

    @Transactional
    public List<Event> getAllEvents() {
        Query namedQuery = entityManager.createNamedQuery("Event.findAll", Event.class);
        return namedQuery.getResultList();
    }

    @Transactional
    public List<Event> getByEventHashes(List<String> eventHashes){
        TypedQuery<Event> namedQuery = entityManager.createNamedQuery("Event.getByEventHashes", Event.class);
        return namedQuery.setParameter("eventHashes", eventHashes).getResultList();
    }

    @Transactional
    public Event addEvent(String name, String description) {
        Event newEvent = new Event();
        newEvent.setDescription(description);
        newEvent.setName(name);
        entityManager.persist(newEvent);
        return newEvent;
    }

    @Transactional
    public void addEvents(List<Event> newEvents) {
        for (Event newEvent : newEvents) {
            entityManager.persist(newEvent);
        }
    }
}