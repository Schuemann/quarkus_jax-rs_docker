package client;

import api.rest.RessourcePaths;

import javax.swing.text.html.HTMLDocument;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class exports the html-view which could (in future) communicate with class @API
 *
 * Please use the web-client in the root-directory instead of this class
 */

@Path(RessourcePaths.APP)
public class WebApp {

    Logger logger = Logger.getLogger(getClass().getName());

    @GET
    @Produces(MediaType.TEXT_HTML)
    public InputStream hello() throws WebApplicationException {
        File f = getFileFromSomewhere();
        try {
            return new FileInputStream(f);
        } catch (FileNotFoundException e) {
            logger.log(Level.SEVERE, "hello went wrong", e);
        }
        return null;
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path(RessourcePaths.IMPRESS)
    public InputStream getImpress() throws WebApplicationException {
        File f = getFileFromSomewhere();
        try {
            return new FileInputStream(f);
        } catch (FileNotFoundException e) {
            logger.log(Level.SEVERE, "hello went wrong", e);
        }
        return null;
    }

    private File getFileFromSomewhere(){
        String path = System.getProperty("user.dir") + "/classes/static/view/impress.html";
        File f = new File(path);
        return f;
    }

}
