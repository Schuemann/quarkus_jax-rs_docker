package api.rest;

import api.dto.EventDTO;
import business_logic.EventService;
import entities.Event;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

/**
 *  Data-Transfer goes via api, responsive website-creation via webclient.WebApp
 * */

@Path(RessourcePaths.API)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class EventRessource {

    @Inject
    EventService eventService;

    @GET
    @Path("/getAllEvents")
    public List<EventDTO> allEvents() throws WebApplicationException {
        List<EventDTO> dtoList = new ArrayList<>();
        eventService.getAllEvents().forEach(evDTO -> dtoList.add(new EventDTO(evDTO)));
        return dtoList;
    }

    @POST
    @Path("/addEvent")
    public EventDTO createEvent(EventDTO eventDTO) {
        Event event = eventService.addEvent(eventDTO.getName(), eventDTO.getDescription());
        return new EventDTO(event); // NEVER expose your real entities via api, also - changes on entities would lead to changes in your api - never change your api!
    }

}
