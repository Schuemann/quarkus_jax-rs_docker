package api.rest;

public class RessourcePaths {

    // for usage in @Path(...)
    public static final String API = "/api";
    public static final String APP = "/app";
    public static final String IMPRESS = "/impress"; //

    // ...
    public static final String IMPRESS_GLOBAL = APP + IMPRESS;

    public static final String PAGE_404 = "";

    // html-file paths also here referencing?

}
