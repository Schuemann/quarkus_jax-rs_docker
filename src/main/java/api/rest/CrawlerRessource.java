package api.rest;


import business_logic.EventService;
import business_logic.SwingCrawlerService;
import entities.Event;
import io.vertx.core.json.Json;
import org.eclipse.yasson.internal.model.JsonbCreator;

import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *  If called, triggers crawlers to crawl
 * */

@Path("/crawler")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CrawlerRessource {

    @Inject
    SwingCrawlerService swingCrawlerService;
    @Inject
    EventService eventService;

    Logger logger = Logger.getLogger(getClass().getName());

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "here are the fruits";
    }

    @GET
    @Path("/startCrawling")
    public Response startCrawling() throws Exception {
        try {
            eventService.addEvents(swingCrawlerService.crawlEventSites());
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error crawling Swing", e);
            return Response.serverError().build();
        }
        return Response.ok().build();
    }

}
