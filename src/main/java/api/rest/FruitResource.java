package api.rest;

import entities.Event;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.time.LocalDate;
import java.util.*;

@Path("/fruits")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class FruitResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "here are the fruits";
    }

    private List<Event> events = new ArrayList<>();

    public FruitResource() throws Exception {
        Date now = new Date(System.currentTimeMillis());
        events.add(new Event(Long.valueOf(2), "Tekkno", "Tekkno-Party", now, now));
        events.add(new Event(Long.valueOf(3), "Reggae", "Reggae-Party", now, now));
    }

    @GET
    @Path("/getAll")
    public List list() {
        return events;
    }

    @POST
    @Path("/addFruit")
    public List add(Event event) {
        events.add(event);
        return events;
    }

    @DELETE
    @Path("/removeFruit")
    public List delete(String eventName) {
        events.removeIf(existingFruit -> existingFruit.getName().toLowerCase().equals(eventName.toLowerCase()));
        return events;
    }
}