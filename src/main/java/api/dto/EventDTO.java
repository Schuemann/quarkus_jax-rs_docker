package api.dto;

import entities.Event;
import javax.json.bind.annotation.JsonbProperty;
import java.io.Serializable;
import java.util.Date;

/**
 * DTO for exchanging simple data with client - part of api!
 * */
public class EventDTO implements Serializable {

    @JsonbProperty("id")
    Long id;
    @JsonbProperty("name")
    String name;
    @JsonbProperty("description")
    String description;
    @JsonbProperty("dateStart")
    Date dateStart;
    @JsonbProperty("dateEnd")
    Date dateEnd;

    public EventDTO(){}

    public EventDTO (Event event){
        this.description = event.getDescription();
        this.name = event.getName();
        this.id = event.getId();
        this.dateStart =event.getDateStart();
        this.dateEnd = event.getDateEnd();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Event createEventEntity() throws Exception {
        return new Event(id, name, description, dateStart, dateEnd);
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }
}
